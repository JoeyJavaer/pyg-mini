// pages/auth/index.js
import regeneratorRuntime from "../../lib/runtime/runtime.js";
import { login } from "../../utils/asyncWx.js";
import { request } from "../../request/index.js"
Page({
  async handleGetUserInfo(e) {

    try {
      //获取用户信息
      const { encryptedData, iv, rawData, signature } = e.detail;
      const { code } = await login();

      //发送获取用户token的请求
      const loginParams = { encryptedData, iv, rawData, signature, code }

      //没有企业账号这里请求获取不到正确结果
      // const {token} = await request({url:"/users/wxlogin",data:loginParams,method:"post"})

      const token = "abcdefghijklmnopqrs";
      // 保存token值
      wx.setStorageSync("token", token);

      // 返回上一层页面
      wx.navigateBack({
        delta: 1
      });
    } catch (error) {
      console.log(error);
    }


  }
})