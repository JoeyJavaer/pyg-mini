// pages/feedback/index.js
import {
  showToast
} from "../../utils/asyncWx.js"
Page({

  data: {
    tabs: [{
        index: 0,
        value: "体验问题",
        isActive: true
      },
      {
        index: 1,
        value: "商品/商家投诉",
        isActive: false
      },
    ],

    choosedImgs: [],
    inputValue: ''
  },

  // 外网的图片路径
  UploadImges: [],

  handleItemTap(e) {
    const {
      index
    } = e.detail;

    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false)
    this.setData({
      tabs
    })
  },

  handleChooseImg() {
    //调用小程序的选择图片
    wx.chooseImage({
      count: 9, //最多可以选择的图片数量
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: (result) => {
        this.setData({
          choosedImgs: [...this.data.choosedImgs, ...result.tempFilePaths]
        })
      },
      fail: () => {},
      complete: () => {}
    });
    // 获取到图片路径的数组 保存到data中 ，展示

  },

  handleremoveImg(e) {

    const {
      index
    } = e.currentTarget.dataset;
    console.log(index);
    let {
      choosedImgs
    } = this.data;
    choosedImgs.splice(index, 1);
    this.setData({
      choosedImgs
    })
  },

  handleInput(e) {
    // console.log(e);
    this.setData({
      inputValue: e.detail.value
    })
  },
  handleCommit() {
    /**
     * 1获取收入内容  对内容的合法性进行校验
     * 2验证图片 (遍历图片数组，挨个上传，自己在维护图片地址数组)到专门的服务器，返回图片外网的链接
     * 3信息提交
     * 4清空页面信息，并返回到上一级页面
     */

    const {
      inputValue,
      choosedImgs
    } = this.data;
    if (!inputValue.trim()) {
      showToast({
        title: "请输入内容"
      })
      return;
    }

    if (choosedImgs.length !== 0) {
      // 上传文件不支持多个文件，只能遍历数组，挨个上传
      choosedImgs.forEach((v, i) => {
        wx.uploadFile({
          url: 'https://images.ac.cn/Home/Index/UploadAction/', //上传的地址
          filePath: v, //文件路径
          name: "file", //上传的文件名称，后台获取文件的file
          formData: {}, //文本信息
          success: (result) => {
            try {
              let url = JSON.parse(result.data).url;
              this.UploadImges.push(url);
            } catch (error) {
              console.log(error)
            }



            //所有文件都长传完毕
            if (i === choosedImgs.length - 1) {
              //       
              setTimeout(() => {
                console.log("提交数字和图片到自己服务器");
                showToast({
                  title: "提交成功"
                });
                this.setData({
                  inputValue: '',
                  choosedImgs: []
                })
                wx.navigateBack({
                  delta: 1
                });
              }, 1500);
            }
          },

        });
      })
    }else{
      showToast({
        title: "提交成功"
      });
      this.setData({
        inputValue: '',
      })
      wx.navigateBack({
        delta: 1
      });
    }  
  }
})