/**
 * 1.输入绑定事件
 *  获取合法
 * 
 * 2.防抖  定时器
 *    1.定义全局定时器ID
 */
import { request } from "../../request/index.js"
import regeneratorRuntime from "../../lib/runtime/runtime.js";
Page({


  data: {
    goods: [],
    query: '',
    //anniu
    isFocus:false,
  
  },

  TimeId: -1,

  handleInput(e) {
    let query = e.detail.value
    if (!query.trim()) {  //飞空判断
      this.setData({
        goods:[],
        isFocus:false,
        query:''
      })
      return;
    }
    this.setData({isFocus:true})
    clearTimeout(this.TimeId);
    this.TimeId=setTimeout(() => {
      this.qsearch(query);
    }, 1000);
 

  },
  async qsearch(query) {
    const res = await request({ url: "/goods/qsearch", data: { query } })
    this.setData({ goods: res })

  },

  handlerCancel(){
    this.setData({
      query:'',
      goods:[],
      isFocus:false,
    
    })
  }

})