Page({


  data: {
    tabs: [{
      index: 0,
      value: "商品收藏",
      isActive: true
    },
    {
      index: 1,
      value: "品牌收藏",
      isActive: false
    },
    {
      index: 2,
      value: "店铺收藏",
      isActive: false
    },
    {
      index: 3,
      value: "浏览足迹",
      isActive: false
    },
    ],
    collection:[]
  },

  onShow() {
    let pages = getCurrentPages();
    let currentPages = pages[pages.length - 1];
    const {
      type
    } = currentPages.options;

    //觉得被激活的数组
    this.changeTitleByIndex(type - 1);

    let collection=wx.getStorageSync("collection")||[];
    this.setData({collection})
  },
  handleItemTap(e) {
    const {
      index
    } = e.detail;
    this.changeTitleByIndex(index);

  },
  changeTitleByIndex(index) {
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false)
    this.setData({
      tabs
    })
  },


})