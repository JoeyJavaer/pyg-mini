/**
 * onShow 不同于onLoad 无法获取到url传递的参数
 * 
 * 判断缓存中有没有token，如果没哟就跳到授权页面，如果有，则获取数据
 */
import {
  request
} from "../../request/index.js"
import regeneratorRuntime from "../../lib/runtime/runtime.js"
Page({

  data: {
    tabs: [{
        index: 0,
        value: "全部",
        isActive: true
      },
      {
        index: 1,
        value: "待付款",
        isActive: false
      },
      {
        index: 2,
        value: "待发货",
        isActive: false
      },
      {
        index: 3,
        value: "退货/退款",
        isActive: false
      },
    ],
    orders: {}
  },

  onShow(options) {
    const token = wx.getStorageSync("token");
    //TODO 放开检查token
    // if (!token) {
    //   wx.navigateTo({
    //     url: '/pages/auth/index'
    //   });

    //   return;
    // }

    //获取小程序的页面栈-数组 ，深度最大是10个页面
    let pages = getCurrentPages();
    let currentPages = pages[pages.length - 1];
    const {
      type
    } = currentPages.options;
    this.getOrders(type);

    //觉得被激活的数组
    this.changeTitleByIndex(type - 1)
  },

  async getOrders(type) {
    const res = await request({
      url: "/my/orders/all",
      data: {
        type
      }
    })

    this.setData({
      orders: res.orders.map((v,i)=>({...v,create_time_cn:new Date(v.create_time*1000).toLocaleString()}))
    })
  },

  changeTitleByIndex(index) {
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false)
    this.setData({
      tabs
    })
  },
  handleItemTap(e) {
    const {
      index
    } = e.detail;
    this.changeTitleByIndex(index);
    this.getOrders(index + 1);
  }


})