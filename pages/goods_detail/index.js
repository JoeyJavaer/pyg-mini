/**
 * 1.发送请求获取数据
 * 2.点击轮播图，大图预览 previewImage
 * 3.点击加入购物车
 *  绑定点击事件
 *  获取缓存中的购物车数据  数组
 *  判断当前商品是否存在，存在数量加，把购物车数据填充到缓存中      不存在添加商品，包括数量  放回缓存
 *  弹出提示
 */
import {
  request
} from "../../request/index.js"
import regeneratorRuntime from "../../lib/runtime/runtime.js";
Page({
  data: {
    goodsInfo: {},
    isCollected: false
  },

  GoodsInfo: {},


  onShow() {
    let pages = getCurrentPages();
    let curretnPage = pages[pages.length - 1];
    const {
      goods_id
    } = curretnPage.options
    this.getGoodsDetail(goods_id);
  },

  async getGoodsDetail(goods_id) {
    const res = await request({
      url: "/goods/detail?",
      data: {
        goods_id
      }
    });
    this.GoodsInfo = res;

    let collections = wx.getStorageSync("collection") || [];
    //判断商品是否是收藏的
    let isCollected = collections.some(v => v.goods_id === this.GoodsInfo.goods_id);
  

    this.setData({
      goodsInfo: {
        goods_name: res.goods_name,
        goods_price: res.goods_price,
        //iphone 部分手机不识别webp图片格式，需要将其替换，前端的解决方案： 将 .webp 替换为 .jpg 要求后台确定有相同的文件
        // goods_introduce:res.goods_introduce.replace(/\.webp/g,'.jpg'),
        goods_introduce: res.goods_introduce,
        pics: res.pics,
        goods_id:goods_id,
        
      },
      isCollected
    })
  },

  handleCollect(){
    let isCollected=false;
    let collections=wx.getStorageSync("collection")||[];
    let index=collections.findIndex(v=> v.goods_id===this.GoodsInfo.goods_id);

    if(index===-1){  //将商品加入到缓冲中的收藏列表中
      collections.push(this.GoodsInfo);
      isCollected=true;
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true
      });
    }else{
      //将商品从收藏列表移除
        collections.splice(index,1);
        isCollected=false;
        wx.showToast({
          title: '取消成功',
          icon: 'success',
          mask: true
        });
    }
    wx.setStorageSync("collection",collections);
    this.setData({isCollected})
  },

  //轮播图放大预览
  handlePreviewImage(e) {
    const urls = this.GoodsInfo.pics.map(v => v.pics_mid)
    const current = e.currentTarget.dataset.url;
    //1构造要预览的图片数组
    // console.log(current);
    wx.previewImage({
      current,
      urls
    });
  },

  handlePurchase(){
    this.GoodsInfo.num = 1;
    wx.setStorageSync('purchase', this.GoodsInfo);
    wx.navigateTo({
      url: '/pages/pay/index?from=detail'});
  },
  handleCartAdd() {
    let cart = wx.getStorageSync('cart') || []; //如果是空的则转为[]
    let index = cart.findIndex(v => v.goods_id === this.GoodsInfo.goods_id);
    if (index === -1) { //商品不存在
      this.GoodsInfo.num = 1;
      this.GoodsInfo.checked = true;
      cart.push(this.GoodsInfo)
    } else { //商品存在
      cart[index].num++;
    }

    wx.setStorageSync("cart", cart);
    wx.showToast({
      title: '加入成功',
      icon: 'success',
      mask: true,
    });
  }
})