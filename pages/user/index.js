// pages/user/index.js
Page({
    data:{
      userInfo:{},
      collectionNum:0
    },

    onShow(){
        const userInfo = wx.getStorageSync("userInfo");
        const collections=wx.getStorageSync("collection")||[];
        let collectionNum=collections.length;


        this.setData({userInfo,collectionNum})
    }
  
})