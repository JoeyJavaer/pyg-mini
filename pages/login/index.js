// pages/login/index.js
Page({

  handlrGetUserInfoResult(res) {
    const {
      userInfo
    } = res.detail;
    wx.setStorageSync("userInfo", userInfo);
    console.log(userInfo);

    wx.navigateBack({
      delta: 1
    });

  }
})