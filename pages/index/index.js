//0 引入发送请求的方法
import { request } from "../../request/index.js"

Page({
  data: {
    swiperList: [],
    catesList: [],
    floorList:[]
  },

  onLoad: function () {
    this.getSwiperList();
    this.getCatitemList();
    this.getFloorData()
  },

  getSwiperList() {
    //优化回调地狱，使用es6 的promise
    // wx.request({
    //   url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
    //   success: (result) => {
    //     console.log(result)
    //     this.setData({
    //       swiperList :result.data.message
    //     })
    //   },
    //   fail: () => { },
    //   complete: () => { }
    // });
    request({
      url: '/home/swiperdata'
    }).then(result => {
      let swiperList=result;
      swiperList.forEach((v,i)=>{
        v.navigator_url=v.navigator_url.replace("main","index");
      });

      this.setData({
        swiperList: result
      })
    })
  },

  getCatitemList() {
    request({
      url: '/home/catitems'
    }).then(result => {
      this.setData({
        catesList: result
      })
    })
  },
  getFloorData(){
    request({
      url:'/home/floordata'
    }).then(res=>{

      let floorList=res;
      
      floorList.forEach((v,i)=>{
          const list=v.product_list;
          // console.log(list);
          list.forEach((v1,i1)=>{
            // console.log(v1.navigator_url.replace("goods_list","goods_list/index"));
            v1.navigator_url=v1.navigator_url.replace("goods_list","goods_list/index");
            
          })
      });

      this.setData({
        floorList:floorList
      })
    })
  }

})
