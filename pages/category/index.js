import { request } from "../../request/index.js"
import regeneratorRuntime from "../../lib/runtime/runtime.js";
Page({

  data: {
    leftMenuList: [],
    rightContent: [],

    //被激活的左侧菜单
    currentIndex: 0,
    scrollTop: 0, //右侧内容距离顶部的距离
  },
  Cates: [],

  onLoad: function (options) {
    /**
     * 判断本地存储中有没有旧数据
     *  web和小程序本地存储的差别
     *   1.代码  web localStorage.setItem(key,value) localStorage.getItem(key)
     *           小程序wx.getStorageSync(key，value) wx.getStorageSync(key)
     *    2.存储的时候，有没有类型转换
     *      web中存入的数据都会先调用toString()，将数据变成字符串存入  小程序中 不存在类型转换，存入数据和获取的数据类型一样
     * 没有旧数据直接发送数据
     * 有旧数据，并且未过期，则拿本地存储的{time:Date.now(),data:[..]}
     */
    //1 获取本地存储的数据
    const Cates = wx.getStorageSync("cates");

    // 判断
    if (!Cates) {
      //不存在，获取网络数据
      this.getCategoryList()
    } else {
      if (Date.now() - Cates.time > 1000 * 10) {//时间差超过十秒就获取新的数据
        this.getCategoryList()
      } else {
        //数据没有过期，可以直接使用
        this.Cates = Cates.data


        let leftMenuList = this.Cates.map(v => v.cat_name);
        let rightContent = this.Cates[0].children;
        this.setData({
          // leftMenuList:leftMenuList
          leftMenuList,
          rightContent
        })
      }
    }


  },
  async getCategoryList() {
    // request({
    //   url: '/categories'
    // }).then(res => {
    //   this.Cates = res.data.message;

    //   //把接口的数据，存入到本地存储中
    //   wx.setStorageSync("cates",{time:Date.now(),data:this.Cates})

    //   //构造左侧分了信息
    //   let leftMenuList= this.Cates.map(v=> v.cat_name);

    //   //构造右侧的商品数据
    //   let rightContent= this.Cates[0].children;
    //   this.setData({
    //     // leftMenuList:leftMenuList
    //     leftMenuList,
    //     rightContent
    //   })
    // })
    const res = await request({ url: '/categories' });
    this.Cates = res;

    //把接口的数据，存入到本地存储中
    wx.setStorageSync("cates", { time: Date.now(), data: this.Cates })

    //构造左侧分了信息
    let leftMenuList = this.Cates.map(v => v.cat_name);

    //构造右侧的商品数据
    let rightContent = this.Cates[0].children;
    this.setData({
      // leftMenuList:leftMenuList
      leftMenuList,
      rightContent
    })
  

  },

handleItemTap(e){
  // 1获取被点击的标题的索引  2 更新currentIndex  3 根据索引设置右侧数据
  console.log(e)
  const { index } = e.currentTarget.dataset;

  let rightContent = this.Cates[index].children;
  this.setData({
    currentIndex: index,
    rightContent,

    //设置scrollview距离顶部的距离
    scrollTop: 0
  })


}


})