
/**
 *  1  滚动到底部，加载下一页数据
 *     没有下一页数据，弹出提示  获取到数据总页数，获取当前的页码，判断当前页码师是否大于等于总页数，则没有更多数据（total是总条数，可以计算得到页数）
 *    有下一页数据，加载数据
 * 
 * 2 下来刷新页面
 *    触发下来刷新事件  在json中开启配置项
 *  1.数据列表清除  页码是1 
 *  2。请求数据
 *  请求完成后关闭下拉刷新的窗口
 *  */ 
import { request } from "../../request/index.js"
import regeneratorRuntime from "../../lib/runtime/runtime.js";

Page({
  data: {
    tabs: [{ id: 0, value: "综合", isActive: true }, { id: 1, value: "销量", isActive: false }, { id: 2, value: "价格", isActive: false }],
    goodsList:[]
  },

  queryParams: {
    query: "",
    cid: "",
    pagenum: 1,
    pagesize: 10
  },
  totalPage:1,
  onLoad: function (options) {
    this.queryParams.cid = options.cid||"";
    this.queryParams.query=options.query||"";
    this.getGoodsList()
  },
  //获取商品列表信息
  async getGoodsList() {
    const res = await request({ url: "/goods/search", data: this.queryParams })
    // console.log(res);
    const total= res.total
    this.totalPage= Math.ceil(total / this.queryParams.pagesize)
    // console.log(this.totalPage);
    this.setData({
      goodsList:[...this.data.goodsList,...res.goods]
    })

    //关闭下拉刷新的  如果没有调用下拉刷新，也可以关闭下拉刷新
    wx.stopPullDownRefresh();

   
  },

  //处理子组件传递的点击的事件
  tabsItemChange(e) {
    //获取被点击的索引
    const { index } = e.detail;

    //修改原数组激活状态
    let { tabs } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false)

    //赋值到data中
    this.setData({
      tabs: tabs
    })
  },

  //滚动条触底时间，用于加载下一页
  onReachBottom(){
    //判断有咩有下一页数据
    if(this.queryParams.pagenum >= this.totalPage){ //没有下一页数据
     wx.showToast({
       title: '没有更多数据了'});
      return;
    }else{
      this.queryParams.pagenum++;
      this.getGoodsList()
    }
  },

  //下拉刷新事件
  onPullDownRefresh(){
    this.setData({
      goodsList:[]
    })
    this.queryParams.pagenum=1
    this.getGoodsList()

  }
})