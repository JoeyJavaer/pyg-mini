/**
 * 1缓存中获取购物车数据 ，checke为true的产品
 * 
 * 2微信支付
 *  1.可以支付的账号可以支付  
 *    1）企业账号 &&&
 *    2）企业账号的小程序后台必须给开发者添加白名单
 *        1.一个appid可以同时绑定多个开发者
 *        2.这些开发者可以共用这个appid和他的开发者权限
 * 
 * 3.支持成功之后，手动删除缓冲中被支付的商品
 */
import regeneratorRuntime from "../../lib/runtime/runtime.js";
import { request } from "../../request/index.js"
import { requestPayment, showToast } from "../../utils/asyncWx.js"
Page({
  data: {
    address: {},
    cart: [],
  },

  onShow() {
    let pages =  getCurrentPages();
    let currentPages=pages[pages.length-1]
    const {from}=currentPages.options;
    this.from=from;
    const address = wx.getStorageSync("address");
    let totalPrice = 0;
    let totalNum = 0;

    if(from==='cart'){
     
      //购物车的数据
      let cart = wx.getStorageSync("cart") || [];
      //过滤checked的商品
      cart = cart.filter(v => v.checked)
     
      cart.forEach(v => {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      });
      this.setData({
        address,
        cart:cart,
        totalPrice,
        totalNum,
      });
    }else{
      let cart=[];
      let purchase=wx.getStorageSync("purchase")||{}
      if(purchase){
        totalNum=1;
        totalPrice=purchase.goods_price;
        cart.push(purchase);
        this.setData({
          address,
          cart,
          totalPrice,
          totalNum,
        });
      }
    }

   
  },

  from:'cart',
  async handleOrderPay() {
    try {
      // 1判断缓存中有没有token 如果没有则跳转到授权 ，获取
      const token = wx.getStorageSync("token");

      if (!token) {
        wx.navigateTo({
          url: '/pages/auth/index',
        });
        return;
      }

      //创建订单
      console.log("创建订单");

      const header = { Authorization, token };//请求头
      const order_price = this.data.totalPrice;
      const consignee_addr = this.data.address.all;

      let goods = [];
      const cart = this.data.cart;
      cart.forEach(v => goods.push({
        goods_id: v.goods_id,
        goods_num: v.num,
        goods_price: v.goods_price
      }));


      const orderParams = {
        order_price, consignee_addr, goods
      }
      // 发送请求,获取订单编号
      const { order_number } = await request({ url: "/my/orders/create", method: "POST", data: orderParams, header: header })

      // 发送请求，预支付的接口
      const { pay } = await request({ url: "", data: { ordder_number }, method: "POST", header })

      //发起微信支付  成功返回只有个msg
      await requestPayment(pay);

      // 查询订单的状态
      const res = await request({ url: "/orders/chkOrder", header, data: { order_number }, method: "POST" })
      await showToast({title:"支付成功"})

      // 清除缓存中已经支付的商品
      if(this.from==="detail"){
        let newCart= wx.getStorageSync("cart");
        newCart=newCart.filter(v=>!v.checked)
        wx.setStorageSync("cart", newCart);
      }else{
        wx.setStorageSync("purchase", '');
      }
     
      
      // 跳转到订单页面
      wx.navigateTo({
        url: '/pages/order/index',
      });
    } catch (error) {
      console.log(error);
      await showToast({title:"支付失败"})
      //因为没有企业账号，必然无法支付成功，这里直接报错后跳转到订单页面
      wx.navigateTo({
        url: '/pages/order/index',
      });


    }

  }
})