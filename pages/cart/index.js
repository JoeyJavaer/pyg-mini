/**
 * 1获取用户的收货地址  wx.chooseAddress()
 * 
 * 2获取用户对小程序所授予的获取地址的状态权限 权限状态scope
 *     1，点击的是确定授权  则是true  authSetting  scope.address
 *     3 如果没有调用过授权地址的api则 scope  undifine
 *     2， 如果刚开始权限时取消，则是false ，需要用户自己打开授权页面，获得权限之后重新获取地址
 *  wx.getSetting({
      success: (result) => {
        console.log(result)
        // 获取授权状态，如果发现一些属性名怪异的时候，都要用[]形式来获取属性值
        const scopeAddress = result.authSetting["scope.address"];
        if (scopeAddress === true || scopeAddress === undefined) {
          wx.chooseAddress({
            success: (result1) => {
              console.log(result1)
            }
          });
        } else {
          wx.openSetting({
            success: (result) => {
              //可以调用获取收货地址代码 
              wx.chooseAddress({
                success: (result3) => {
                  console.log(result3)
                }
              });
            }
          });
        }
      },
    });
 * 
 * 2 页面加载完成
 *  获取本地存储中的地址数据
 *  把数据设置给data中的一个变量
 * 
 * 获取购物车数组
 * 
 * 
 * 3总价格和总数量，都是选中状态的
 *  获取购物车数组
 *  遍历循环
 *  总价格
 *  总数量
 * 4 商品的选中
 *  绑定时间
 * 
 * 5点击结算的逻辑
 *  判断是否有收货地址
 *  判断购物车中是否有商品
 *  跳转到支付页面
 */
import regeneratorRuntime from "../../lib/runtime/runtime.js";
import { getSetting, chooseAddress, openSetting, showModal, showToast } from "../../utils/asyncWx.js"

Page({
  data: {
    address: {},
    cart: [],
    allChecked: false
  },

  onShow() {
    const address = wx.getStorageSync("address");

    //购物车
    const cart = wx.getStorageSync("cart") || [];

    //注意：空数组调用了every方法，则返回值是true，
    // const allChecked = cart.length ? cart.every(v => v.checked) : false

    // let allChecked = true;
    // let totalPrice = 0;
    // let totalNum = 0;

    // cart.forEach(v => {
    //   if (v.checked) {
    //     totalPrice += v.num * v.goods_price;
    //     totalNum += v.num;
    //   } else {
    //     allChecked = false;
    //   }
    // });

    // //判断数组是否为空
    // allChecked = cart.length !== 0 ? allChecked : false;

    // this.setData({
    //   address,
    //   cart,
    //   allChecked,
    //   totalPrice: 0,
    //   totalNum: 0,
    //   totalPrice,
    //   totalNum
    // })

    this.setCart(cart);
    this.setData({
      address,
    })

  },

  async handleChooseAddress() {
    try {
      const res1 = await getSetting();
      const scopeAddress = res1.authSetting["scope.address"];
      //判断权限状态
      // if (scopeAddress === true || scopeAddress === undefined) {
      //   //调用获取地址
      // } else {
      //   //诱导用户打开setting，授权
      //   await openSetting()
      // }
      if (scopeAddress === false) {
        await openSetting()
      }
      let address = await chooseAddress();
      address.all = address.provinceName + address.cityName + address.countyName + address.detailInfo;
      //存入到本地
      wx.setStorageSync("address", address);

    } catch (error) {
      console.log(error);
    }
  },

  handlerItemChanged(e) {
    //获取操作的id
    const goods_id = e.currentTarget.dataset.id;
    let { cart } = this.data;
    let index = cart.findIndex(v => v.goods_id === goods_id);
    cart[index].checked = !cart[index].checked;
    this.setCart(cart);
  },

  //设置购物车状态，跟新数据
  setCart(cart) {

    wx.setStorageSync("cart", cart);
    let allChecked = true;
    let totalPrice = 0;
    let totalNum = 0;

    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    });

    //判断数组是否为空
    allChecked = cart.length !== 0 ? allChecked : false;

    //跟新数据
    this.setData({
      cart,
      allChecked,
      totalPrice,
      totalNum,
    });

  },

  // 全选和不选
  handleAllCheckChanged() {
    let { cart, allChecked } = this.data;
    allChecked = !allChecked;
    cart.forEach(v => v.checked = allChecked)
    this.setCart(cart)
  },

  async editItemNum(e) {
    //获取到要编辑的商品id和操作类型
    const { opt, id } = e.currentTarget.dataset;
    let { cart } = this.data;
    const index = cart.findIndex(v => v.goods_id === id)
    if (cart[index].num === 1 && opt === -1) {//当数量是1,并且是-1的时候，弹窗是否删除
      const res = await showModal({ content: "确定删除商品？" })
      if (res.confirm) {
        cart.splice(index, 1);
      }
    } else {
      cart[index].num += opt;
    }
    this.setCart(cart);
  },

  async handlePay() {
    const { address,totalNum} = this.data;
    if (!address.userName) {
      await showToast({title:"未添加收货地址"})
      return;
    } 
    if (totalNum === 0) {
      await showToast({title:"购物车为空"})
      return;
    }
    //跳转到支付页面
    wx.navigateTo({
      url: '/pages/pay/index?from=cart'
    });
  }
})