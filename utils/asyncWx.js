
/**
 * promise 的getSetting
 */
export const getSetting = () => {
  return new Promise((resolve, reject) => {
    wx.getSetting({
      success: (result) => {
        resolve(result)
      },
      fail: (err) => {
        reject(err)
      },

    });
  })
}

/**
 * 获取收货地址
 */
export const chooseAddress = () => {
  return new Promise((resolve, reject) => {
    wx.chooseAddress({
      success: (result) => {
        resolve(result)
      },
      fail: (err) => {
        reject(err)
      },

    });
  })
}

/**
 * 打开小程序的设置
 */
export const openSetting = () => {
  return new Promise((resolve, reject) => {
    wx.openSetting({
      success: (result) => {
        resolve(result)
      },
      fail: (err) => {
        reject(err)
      },

    });
  })
}


/**
 * 小程序的弹窗页面
 */
export const showModal = ({ content }) => {
  return new Promise((resolve, reject) => {
    wx.showModal({
      title: '提示',
      content: content,
      success: (result) => {
        resolve(result)
      }, fail: (error) => {
        reject(error)
      }
    });
  })
}


/**
 * 小程序的toast页面
 */
export const showToast = ({ title }) => {
  return new Promise((resolve, reject) => {
    wx.showToast({
      title: title,
      icon: 'none',
      image: '',
      duration: 1500,
      mask: false,
      success: (result)=>{
        resolve(result)
      },
      fail: (error)=>{reject(error)},
      complete: ()=>{}
    });
  })
}


/**
 * 微信登录
 */
export const login = () => {
  return new Promise((resolve, reject) => {
    wx.login({
      timeout:10000,
      success: (result)=>{
        resolve(result)
      },
      fail: (err)=>{reject(err)},
      complete: ()=>{}
    });
  })
}

/**
 * 调用微信的支付接口
 * @param {发起支付的参数}} pay 
 */
export const requestPayment = (pay) => {
  return new Promise((resolve, reject) => {
   wx.requestPayment({
     ...pay,
     success: (result)=>{
       resolve(result)
     },
     fail: (err)=>{reject(err)},
     complete: ()=>{}
   });
  })
}


